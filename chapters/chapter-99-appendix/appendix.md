# 附錄

## PyCon 註冊商標

**PyCon** 是 Python 軟體基金會 （ Python Software Foundation ， PSF ）註冊商標，並授權給各地區性社群、在符合對應的行為準則下所舉辦的 Python 社群聚會使用。

習慣上不特別說明時， PyCon 指得是由 PSF 主辦的 PyCon ，因為實體地點通常在美國，所以又加上尾綴 *US* ，成為 PyCon US ，以和其他地區性 PyCon 有所區別。

詳細資訊，例如商標使用條款，與全球各地取得授權使用 PyCon 名稱的地區性 PyCon ，可上 PSF 網站 [PSF PyCon Trademark Usage Policy](https://www.python.org/psf/trademarks/pycon/) 取得更多資訊。
